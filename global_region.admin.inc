<?php
function global_region_settings() {

  $form['global_region_position'] = array(
    '#type' => 'select',
    '#title' => t('Region Position'),
    '#default_value' => variable_get('global_region_position', 'horizontal'),
    '#options' => array(
      'top' => 'Top',
      'left' => 'Left',
    ),
    '#description' => t('Should the global region be display at the top of the page or vertically on the left'),
  );
  $form['global_region_size'] = array(
    '#type' => 'textfield',
    '#title' => t('Global Region Size'),
    '#default_value' => variable_get('global_region_size', '100'),
    '#description' => t('Set the width(for vertical) or height(for horizontal) in pixels.'),
  );
  $form['global_region_position_fixed'] = array(
    '#type' => 'checkbox',
    '#title' => t('Keep the Global Region\'s position fixed'),
    '#default_value' => variable_get('global_region_position_fixed', 0),
    '#description' => t('If enabled, the Global Region\'s position will be fixed (even after the page is scrolled). <strong>Note: In some browsers, this setting results in a malformed page, an invisible cursor, non-selectable elements in forms, or other issues. Disable this option if these issues occur.</strong>'),
  );
  $form['vertical'] = array(
    '#type' => 'fieldset',
    '#title' => t('Top Only Settings'),
    '#description' => t('These settings only apply if you are using the vertical orientation.'),
  );
  $form['vertical']['global_region_margin_top'] = array(
    '#type' => 'checkbox',
    '#title' => t('Adjust top margin'),
    '#default_value' => variable_get('global_region_margin_top', 1),
    '#description' => t('If enabled, the site output is shifted down approximately 20 pixels from the top of the viewport to display the Global Region. If disabled, some absolute- or fixed-positioned page elements may be covered by the administration menu.'),
  );

  $form['global_region_css'] = array(
    '#type' => 'textarea',
    '#title' => t('Additional CSS'),
    '#default_value' => variable_get('global_region_css', ''),
    '#description' => t('Additional CSS to affect the Global Region.  Use the "#global-region" id to target the global region.'),
  );
  $form = system_settings_form($form);
  return $form;
}
